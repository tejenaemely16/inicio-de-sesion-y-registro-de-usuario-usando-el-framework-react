import React, { useState } from 'react';  // Importa React y useState desde React.
import '../css/login.css';  // Importa una hoja de estilos CSS.

// Define un componente funcional llamado "Login" que recibe "props".
export const Login = (props) => {
  // Define tres estados locales para el componente: username, password y message.
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [message, setMessage] = useState('');

  // Función que se llama cuando se envía el formulario de inicio de sesión.
  const handleSubmit = (e) => {
    e.preventDefault();

    // Validar las credenciales ingresadas
    const usuarios = JSON.parse(localStorage.getItem('usuarios')) || [];
    const usuarioEncontrado = usuarios.find(
      (user) => user.username === username && user.password === password
    );

    // Si se encuentra un usuario con las credenciales ingresadas, muestra un mensaje de éxito y redirige a la página de perfil.
    if (usuarioEncontrado) {
      showMessage('Inicio de sesión exitoso', 'success');
      // Redirigir al usuario a la página de perfil
      window.location.href = 'perfil.html';
    } else {
      // Si no se encuentra un usuario, muestra un mensaje de error.
      showMessage('Credenciales incorrectas. Por favor, inténtelo nuevamente', 'error');
    }
  };

  // Función para mostrar un mensaje y alerta.
  const showMessage = (text, type) => {
    setMessage(text);
    if (type === 'success') {
      alert('Inicio de sesión exitoso.');
    } else if (type === 'error') {
      alert('Credenciales incorrectas. Por favor, inténtelo nuevamente.');
    }
  };

  // Funciones para cambiar entre diferentes formularios.
  const handleRegresarIndex = () => {
    props.onFormSwitch('main');
  };

  const handleRegresarRegistro = () => {
    props.onFormSwitch('registro');
  };

  const handleRegresarAdmin = () => {
    props.onFormSwitch('login2');
  };

  // Renderiza el formulario de inicio de sesión y otros elementos HTML.
  return (
    <div className="container">
      <h2>Iniciar Sesión</h2>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="username">Nombre de usuario:</label>
          <input
            type="text"
            id="username"
            name="username"
            required
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </div>
        <div className="form-group">
          <label htmlFor="password">Contraseña:</label>
          <input
            type="password"
            id="password"
            name="password"
            required
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <button type="submit">Iniciar Sesión</button>
      </form>
      <div id="message">{message}</div>
      <div className="buttons">
        <button
          type="button"
          onClick={handleRegresarIndex}
        >
          Regresar a la pestaña principal
        </button>
        <button
          type="button"
          onClick={handleRegresarRegistro}
        >
          ¿No tienes cuenta? Regístrate
        </button>
        <button
          type="button"
          onClick={handleRegresarAdmin}
        >
          Iniciar sesión como administrador
        </button>
      </div>
    </div>
  );
};
