import React, { useState } from 'react';  // Importa React y useState desde React.
import '../css/registro.css';  // Importa una hoja de estilos CSS.

// Define un componente funcional llamado "Registro".
export const Registro = () => {
  // Define varios estados locales para el componente: username, nombres, apellidos, correo, celular, edad, password, confirmPassword y message.
  const [username, setUsername] = useState('');
  const [nombres, setNombres] = useState('');
  const [apellidos, setApellidos] = useState('');
  const [correo, setCorreo] = useState('');
  const [celular, setCelular] = useState('');
  const [edad, setEdad] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [message, setMessage] = useState('');

  // Función que se llama al enviar el formulario de registro.
  const handleSubmit = (e) => {
    e.preventDefault();

    // Validaciones para los campos del formulario.
    // Verifica que todos los campos estén completos.
    if (!username || !nombres || !apellidos || !correo || !celular || !edad || !password || !confirmPassword) {
      showMessage("Por favor, complete todos los campos.", "error");
      return;
    }

    // Verifica que los campos de nombres y apellidos contengan solo letras, espacios y caracteres acentuados.
    if (!/^[a-zA-ZÁÉÍÓÚÜáéíóúü\s]+$/.test(nombres) || !/^[a-zA-ZÁÉÍÓÚÜáéíóúü\s]+$/.test(apellidos)) {
      showMessage("Los campos 'nombres' y 'apellidos' solo pueden contener letras, espacios y caracteres acentuados.", "error");
      return;
    }

    // Verifica que la contraseña tenga al menos 3 caracteres.
    if (password.length < 3) {
      showMessage("La contraseña debe tener al menos 3 caracteres.", "error");
      return;
    }

    // Verifica que las contraseñas coincidan.
    if (password !== confirmPassword) {
      showMessage("Las contraseñas no coinciden.", "error");
      return;
    }

    // Verifica que la edad sea un número entre 1 y 99.
    if (isNaN(edad) || edad < 1 || edad > 99) {
      showMessage("La edad debe ser un número entre 1 y 99.", "error");
      return;
    }

    // Verifica que el correo electrónico sea válido.
    const correoVali = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
    if (!correoVali.test(correo)) {
      showMessage("Ingrese una dirección de correo electrónico válida.", "error");
      return;
    }

    // Verifica que el número de celular tenga 10 dígitos.
    const celularVali = /^\d{10}$/;
    if (!celularVali.test(celular)) {
      showMessage("Ingrese un número de teléfono válido (10 dígitos).", "error");
      return;
    }

    // Verifica que el nombre de usuario tenga entre 3 y 20 caracteres y contenga solo letras, números y guiones bajos.
    if (username.length < 3 || username.length > 20) {
      showMessage("El nombre de usuario debe tener entre 3 y 20 caracteres.", "error");
      return;
    }
    const usuarioVali = /^[a-zA-Z0-9_]+$/;
    if (!usuarioVali.test(username)) {
      showMessage("El nombre de usuario solo puede contener letras, números y guiones bajos.", "error");
      return;
    }

    // Si todas las validaciones son exitosas, crea un objeto de usuario y lo almacena en el almacenamiento local.
    const usuario = {
      username,
      nombres,
      apellidos,
      correo,
      celular,
      edad,
      password
    };
    const usuarios = [usuario];
    localStorage.setItem("usuarios", JSON.stringify(usuarios));

    showMessage("Registro exitoso.", "success");
  };

  // Función para mostrar un mensaje.
  const showMessage = (text, type) => {
    setMessage(type === 'error' ? 'Error: ' + text : 'Éxito: ' + text);
    alert(message);
  };

  // Funciones para redirigir a diferentes páginas.
  const handleRegresarIndex = () => {
    window.location.href = 'index.html';
  };

  const handleRegresarInicioSesion = () => {
    window.location.href = 'login.html';
  };

  // Renderiza el formulario de registro y otros elementos HTML.
  return (
    <div className="container">
      <h2>Registro de Sesión</h2>
      <form onSubmit={handleSubmit}>
        {/* Campos del formulario */}
        <div className="form-group">
          <label htmlFor="username">Nombre de usuario:</label>
          <input
            type="text"
            id="username"
            name="username"
            required
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </div>
        <div className="form-group">
          <label htmlFor="nombres">Nombres:</label>
          <input
            type="text"
            id="nombres"
            name="nombres"
            required
            value={nombres}
            onChange={(e) => setNombres(e.target.value)}
          />
        </div>
        <div className="form-group">
          <label htmlFor="apellidos">Apellidos:</label>
          <input
            type="text"
            id="apellidos"
            name="apellidos"
            required
            value={apellidos}
            onChange={(e) => setApellidos(e.target.value)}
          />
        </div>
        <div className="form-group">
          <label htmlFor="correo">Correo electrónico:</label>
          <input
            type="email"
            id="correo"
            name="correo"
            required
            value={correo}
            onChange={(e) => setCorreo(e.target.value)}
          />
        </div>
        <div className="form-group">
          <label htmlFor="celular">Número de celular:</label>
          <input
            type="tel"
            id="celular"
            name="celular"
            required
            value={celular}
            onChange={(e) => setCelular(e.target.value)}
          />
        </div>
        <div className="form-group">
          <label htmlFor="edad">Edad (1-99):</label>
          <input
            type="number"
            id="edad"
            name="edad"
            min="1"
            max="99"
            required
            value={edad}
            onChange={(e) => setEdad(e.target.value)}
          />
        </div>
        <div className="form-group">
          <label htmlFor="password">Contraseña:</label>
          <input
            type="password"
            id="password"
            name="password"
            required
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <div className="form-group">
          <label htmlFor="confirm-password">Confirmar contraseña:</label>
          <input
            type="password"
            id="confirm-password"
            name="confirm-password"
            required
            value={confirmPassword}
            onChange={(e) => setConfirmPassword(e.target.value)}
          />
        </div>
        <button type="submit">Registrar</button>
      </form>
      <div id="message">{message}</div>
      <div className="buttons">
        <button
          type="button"
          onClick={handleRegresarIndex}
        >
          Regresar a Index
        </button>
        <button
          type="button"
          onClick={handleRegresarInicioSesion}
        >
          Regresar a Inicio de Sesión
        </button>
      </div>
    </div>
  );
};
