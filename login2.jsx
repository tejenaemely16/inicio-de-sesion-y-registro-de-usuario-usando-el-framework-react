import React, { useState } from 'react';  // Importa React y useState desde React.
import '../css/login2.css';  // Importa una hoja de estilos CSS.

// Define un componente funcional llamado "AdminLogin" que recibe "props".
export const AdminLogin = (props) => {
  // Define tres estados locales para el componente: useradmin, password y message.
  const [useradmin, setUseradmin] = useState('');
  const [password, setPassword] = useState('');
  const [message, setMessage] = useState('');

  // Función para validar el formulario de inicio de sesión del administrador.
  const validarFormulario = () => {
    if (useradmin === 'admin' && password === '123') {
      showMessage('Inicio de sesión exitoso', 'success');
      window.location.href = 'administrador.html';  // Redirige al administrador a la página de administrador.
    } else {
      showMessage('Usuario o contraseña incorrectos. Por favor, inténtelo de nuevo.', 'error');
    }
  };

  // Función para mostrar un mensaje y una alerta.
  const showMessage = (text, type) => {
    setMessage(text);
    if (type === 'success') {
      alert('Inicio de sesión exitoso.');
    } else if (type === 'error') {
      alert('Usuario o contraseña incorrectos. Por favor, inténtelo de nuevo.');
    }
  };

  // Función para cambiar al formulario de inicio de sesión de usuario.
  const handleRegresarUsuario = () => {
    props.onFormSwitch('login1');
  };

  // Renderiza el formulario de inicio de sesión del administrador y otros elementos HTML.
  return (
    <div className="container">
      <h1>Administrador</h1>
      <form onSubmit={validarFormulario}>
        <label htmlFor="useradmin">Usuario:</label>
        <input
          type="text"
          id="useradmin"
          name="useradmin"
          required
          value={useradmin}
          onChange={(e) => setUseradmin(e.target.value)}
        /><br/><br/>
        <label htmlFor="password">Contraseña:</label>
        <input
          type="password"
          id="password"
          name="password"
          required
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        /><br/><br/>
        <input type="submit" value="Iniciar Sesión" />
      </form>
      <div id="message">{message}</div>
      <div className="buttons">
        <button
          type="button"
          onClick={handleRegresarUsuario}
        >
          Iniciar sesión como usuario
        </button>
      </div>
    </div>
  );
};
